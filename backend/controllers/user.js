const User = require('../models/User');
const bcrypt = require('bcrypt')

exports.signup = (req, res, next) => {
    const data = new User({
        ...req.body
    });
    bcrypt.hash(data.password, 10)
    .then(hash => {
        data.password = hash;
        console.log(data.password);
        res.status(200).json({message: "OK"})
    })
    .catch(error => res.status(404).json({error}));
};

exports.login = (req, res, next) => {

};