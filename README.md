# NodeJs, Express and Mongo

## Introduction to the project and choice of stack
Here is a first backend project with the implementation of an API to Create, Read, Update and Delete (CRUD) items on sale.

This is a tutorial provided by OpenClassroom (https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb).

The choice of this stack is not random. Express is a good introduction to NestJS, which is itself an overlay of express. The same goes for MangoDB. It allows to quickly set up a DB and to discover a new technology.

To make the application work, it will be necessary to update the MangoDB identifiers and launch the back-end and front-end applications.

#### Most of the notions learned are in the following files:

```
|-- backend/
|   |-- app.js
|   |-- server.js
|   |-- controllers/
|       |-- stuff.js
|       |-- user.js
|   |-- models/
|       |-- Thing.js
|       |-- User.js
|   |-- routes/
|       |-- stuff.js
|       |-- user.js
```
